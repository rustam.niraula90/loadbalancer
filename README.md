# Client Side Load Balancing Using Spring Boot And Netflix Ribbon
Three mirror endpoints has been exposed which takes request from the client then forwards the request to the service instance
- /login (GET)
    - `Netflix Ribbon` is used to load balance the `GET` request selecting the  server in round-robin fashion
    - It's ping the clients on filxed interval ***(configurable on `application.yml`)*** on `/` endpoint to check if the server is available
- /register (POST)
- /changePassword (POST)
    - With custom implementation the `POST` requests are forwarded to all the servers and return the first successful response.
    - `Spring-retry` is used to retry the request until successful response is received or max-attempt ***(configurable on `appliction.yml`)*** is reached.

To run locally:
````
./gradlew bootRun
````
In your browser, go to [http://localhost:8080/](http://localhost:8080/)
***port configurable in `application.yml`***

Or create a runnable jar file:
````
./gradlew bootJar
````
The jar file is located in `build/libs/loadBalancer-0.0.1-SNAPSHOT.jar`
Deploy the jar file and run it on any machine
```
java -jar build/libs/loadBalancer-0.0.1-SNAPSHOT.jar
```
You can also configure the port :
```
java -jar build/libs/loadBalancer-0.0.1-SNAPSHOT.jar --server.port=8080
```
The detail about the configuration is commented on `application.yml` properties. The configuration file `application.yml` is located at `src.main.resources.config.application.yml`