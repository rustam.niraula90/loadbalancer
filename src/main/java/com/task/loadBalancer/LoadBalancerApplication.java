package com.task.loadBalancer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;

import java.net.InetAddress;
import java.net.UnknownHostException;

@EnableRetry
@EnableAsync
@SpringBootApplication
public class LoadBalancerApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoadBalancerApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        SpringApplication app = new SpringApplication(LoadBalancerApplication.class);
        logApplicationDetail(app.run(args).getEnvironment());
    }

    private static void logApplicationDetail(Environment env) throws UnknownHostException {
        String protocol = "http";
        LOGGER.info("\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running!\n\t" +
                        "Local URL: \t\t{}://localhost:{}\n\t" +
                        "External URL: \t{}://{}:{}\n" +
                        "----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                protocol,
                env.getProperty("server.port"),
                protocol,
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"));
    }

}
