package com.task.loadBalancer.api.v1.authentication;

import com.task.loadBalancer.service.GetService;
import com.task.loadBalancer.service.PostService;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@CrossOrigin
public class AuthenticationRestController {

    private final GetService getService;

    private final PostService postService;

    private final Environment env;

    public AuthenticationRestController(GetService getService, PostService postService, Environment env) {
        this.getService = getService;
        this.postService = postService;
        this.env = env;
    }

    @GetMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public String login() {
        return getService.login();
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public String register() throws ExecutionException, InterruptedException {
        List<String> serverList = getServerList();
        return requestRegisterToAllServer(serverList);
    }

    @PostMapping("/changePassword")
    @ResponseStatus(HttpStatus.CREATED)
    public String changePassword() throws ExecutionException, InterruptedException {
        List<String> serverList = getServerList();
        return requestChangePasswordToAllServer(serverList);
    }

    private String requestChangePasswordToAllServer(List<String> serverList) throws InterruptedException, ExecutionException {
        List<Future<String>> futureList = new ArrayList<>();
        for (String server : serverList)
            futureList.add(postService.changePassword(server));
        return getResponseFromFuture(futureList);
    }

    private String requestRegisterToAllServer(List<String> serverList) throws InterruptedException, ExecutionException {
        List<Future<String>> futureList = new ArrayList<>();
        for (String server : serverList)
            futureList.add(postService.register(server));
        return getResponseFromFuture(futureList);
    }

    private String getResponseFromFuture(List<Future<String>> futureList) throws ExecutionException, InterruptedException {
        while (true) {
            for (Future<String> future : futureList)
                if (future.isDone())
                    return future.get();
            Thread.sleep(1000);
        }
    }

    private List<String> getServerList() {
        return Arrays.asList(Objects.requireNonNull(env.getProperty(env.getProperty("spring.application.name")
                + ".ribbon.listOfServers")).split(","));
    }

}
