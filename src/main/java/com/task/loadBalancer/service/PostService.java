package com.task.loadBalancer.service;

import java.util.concurrent.Future;

public interface PostService {

    Future<String> register(String host);

    Future<String> changePassword(String host);
}
