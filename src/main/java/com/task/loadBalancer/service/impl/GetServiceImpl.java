package com.task.loadBalancer.service.impl;

import com.task.loadBalancer.config.RibbonConfiguration;
import com.task.loadBalancer.service.GetService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RibbonClient(name = "load-balancer", configuration = RibbonConfiguration.class)
public class GetServiceImpl implements GetService {

    private final RestTemplate restTemplate;

    public GetServiceImpl(@Qualifier("LoadBalanced") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public String login() {
        return this.restTemplate.getForObject("http://load-balancer/login", String.class);
    }
}
