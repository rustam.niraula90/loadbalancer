package com.task.loadBalancer.service.impl;

import com.task.loadBalancer.service.PostService;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.ConnectException;
import java.util.concurrent.Future;

@Service
public class PostServiceImpl implements PostService {

    private final RestTemplate restTemplate;

    public PostServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    @Async
    @Retryable(value = {RestClientException.class, ConnectException.class},
            backoff = @Backoff(delayExpression = "${post.backoff.delay}",
                    multiplierExpression = "${post.backoff.multiplier}", random = true),
            maxAttemptsExpression = "${post.backoff.maxAttempts}")
    public Future<String> register(String host) {
        return new AsyncResult<>(this.restTemplate.postForObject("http://" + host + "/register", null, String.class));
    }

    @Override
    @Async
    @Retryable(value = {RestClientException.class, ConnectException.class},
            backoff = @Backoff(delayExpression = "${post.backoff.delay}",
                    multiplierExpression = "${post.backoff.multiplier}", random = true),
            maxAttemptsExpression = "${post.backoff.maxAttempts}")
    public Future<String> changePassword(String host) {
        return new AsyncResult<>(this.restTemplate.postForObject("http://" + host + "/changePassword", null, String.class));
    }
}
