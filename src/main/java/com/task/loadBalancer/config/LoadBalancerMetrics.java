package com.task.loadBalancer.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class LoadBalancerMetrics implements HealthIndicator {

    @Override
    public Health health() {
        //TODO Implement metrics
        return Health.up().withDetail("TODO", "Implement metrics").build();
    }
}
